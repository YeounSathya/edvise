<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'ConstultingController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'ConstultingController@create']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ConstultingController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'ConstultingController@update']);
	
	Route::put('/', 				['as' => 'store', 			'uses' => 'ConstultingController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ConstultingController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'ConstultingController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'ConstultingController@updateStatus']);
});	