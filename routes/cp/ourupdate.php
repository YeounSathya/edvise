<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'OurupdateController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'OurupdateController@create']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'OurupdateController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'OurupdateController@update']);
	
	Route::put('/', 				['as' => 'store', 			'uses' => 'OurupdateController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'OurupdateController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'OurupdateController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'OurupdateController@updateStatus']);
});	