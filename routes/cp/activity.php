<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'ActivityController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'ActivityController@create']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ActivityController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'ActivityController@update']);
	
	Route::put('/', 				['as' => 'store', 			'uses' => 'ActivityController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ActivityController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'ActivityController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'ActivityController@updateStatus']);
});	