<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'AboutbannerController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'AboutbannerController@create']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'AboutbannerController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'AboutbannerController@update']);
	
	Route::put('/', 				['as' => 'store', 			'uses' => 'AboutbannerController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'AboutbannerController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'AboutbannerController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'AboutbannerController@updateStatus']);
});	