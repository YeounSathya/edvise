<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'OurclientController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'OurclientController@create']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'OurclientController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'OurclientController@update']);
	
	Route::put('/', 				['as' => 'store', 			'uses' => 'OurclientController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'OurclientController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'OurclientController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'OurclientController@updateStatus']);
});	