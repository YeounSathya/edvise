<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'WhoweareController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'WhoweareController@create']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'WhoweareController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'WhoweareController@update']);
	
	Route::put('/', 				['as' => 'store', 			'uses' => 'WhoweareController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'WhoweareController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'WhoweareController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'WhoweareController@updateStatus']);
});	