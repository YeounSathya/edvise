<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Slide

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'DirectorController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'DirectorController@create']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'DirectorController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'DirectorController@update']);
	
	Route::put('/', 				['as' => 'store', 			'uses' => 'DirectorController@store']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'DirectorController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'DirectorController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'DirectorController@updateStatus']);
});	