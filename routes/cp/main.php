<?php

	//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Auth
	Route::group(['as' => 'auth.', 'prefix' 		=> 'auth', 'namespace' 			=> 'Auth'], function(){	
		require(__DIR__.'/auth.php');
	});
	
	//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Authensicated
	Route::group(['middleware' => 'authenticatedUser'], function() {
		Route::group(['as' => 'user.',  'prefix' 	=> 'user', 'namespace' 			=> 'User'], function () {
			require(__DIR__.'/user.php');
		});

	Route::group(['as' => 'slide.',  'prefix' 	 	=> 'slide', 'namespace' 		=> 'Slide'], function () {
			require(__DIR__.'/slide.php');
		});
	Route::group(['as' => 'director.',  'prefix'	=> 'director', 'namespace' 		=> 'Director'], function () {
			require(__DIR__.'/director.php');
		});

	Route::group(['as' => 'constulting.',  'prefix' => 'constulting', 'namespace' 	=> 'Constulting'], function () {
			require(__DIR__.'/constulting.php');
		});

	Route::group(['as' => 'ourupdate.',  'prefix' 	=> 'ourupdate', 'namespace' 	=> 'Ourupdate'], function () {
			require(__DIR__.'/ourupdate.php');
		});
	Route::group(['as' => 'partner.',  'prefix' 	=> 'partner', 'namespace' 		=> 'Partner'], function () {
			require(__DIR__.'/partner.php');
		});
	
	Route::group(['as' => 'ourmanagement.',  'prefix' => 'ourmanagement', 'namespace' => 'Ourmanagement'], function () {
			require(__DIR__.'/ourmanagement.php');
		});

	Route::group(['as' => 'whoweare.',  'prefix' 	  	=> 'whoweare', 'namespace' 	 => 'Whoweare'], function () {
			require(__DIR__.'/whoweare.php');
		});
	Route::group(['as' => 'management.',  'prefix' 		=> 'management', 'namespace' => 'Management'], function () {
			require(__DIR__.'/management.php');
		});
	Route::group(['as' => 'aboutbanner.',  'prefix' 	=> 'aboutbanner', 'namespace' => 'Aboutbanner'], function () {
			require(__DIR__.'/aboutbanner.php');
		});
	Route::group(['as' => 'bannerfooter.',  'prefix'    => 'bannerfooter', 'namespace' => 'Bannerfooter'], function () {
			require(__DIR__.'/bannerfooter.php');
		});
	Route::group(['as' => 'ourclient.',  'prefix' 		=> 'ourclient', 'namespace'    => 'Ourclient'], function () {
			require(__DIR__.'/ourclient.php');
		});
	Route::group(['as' => 'activity.',  'prefix' 		=> 'activity', 'namespace' 	   => 'Activity'], function () {
			require(__DIR__.'/activity.php');
		});
	Route::group(['as' => 'massage.',  'prefix' 		=> 'massage', 'namespace' 	   => 'Massage'], function () {
			require(__DIR__.'/massage.php');
		});
	Route::group(['as' => 'service.',  'prefix' 		=> 'service', 'namespace' 	   => 'Service'], function () {
			require(__DIR__.'/service.php');
		});
	Route::group(['as' => 'service_specific.',  'prefix' 		=> 'service_specific', 'namespace' 	   => 'ServiceSpecific'], function () {
			require(__DIR__.'/service_specific.php');
		});

	});