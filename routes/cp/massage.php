<?php

Route::get('/', 				['as' => 'index', 			'uses' => 'MassageController@index']);
Route::get('/create', 			['as' => 'create', 			'uses' => 'MassageController@create']);
Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'MassageController@edit']);
Route::post('/', 				['as' => 'update', 			'uses' => 'MassageController@update']);
Route::put('/', 				['as' => 'store', 			'uses' => 'MassageController@store']);
Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'MassageController@trash']);

