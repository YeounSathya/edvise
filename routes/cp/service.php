<?php
//:::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Service 

Route::group([], function () {
	Route::get('/', 				['as' => 'index', 			'uses' => 'ServiceController@index']);
	Route::get('/create', 			['as' => 'create', 			'uses' => 'ServiceController@create']);
	Route::put('/', 				['as' => 'store', 			'uses' => 'ServiceController@store']);
	Route::get('/{id}', 			['as' => 'edit', 			'uses' => 'ServiceController@edit']);
	Route::post('/', 				['as' => 'update', 			'uses' => 'ServiceController@update']);
	Route::delete('/{id}', 			['as' => 'trash', 			'uses' => 'ServiceController@trash']);
	Route::post('order', 			['as' => 'order', 			'uses' => 'ServiceController@order']);
	Route::post('status', 			['as' => 'update-status', 	'uses' => 'ServiceController@updateStatus']);

	///================= Specific 

	Route::get('/{id}/specific', 			['as' => 'specific', 			'uses' => 'ServiceSpecificController@index']);

	Route::get('/{id}/create-specific', 			['as' => 'create-specific', 			'uses' => 'ServiceSpecificController@create']);
	Route::put('/{id}/create-specific', 				['as' => 'store-specific', 			'uses' => 'ServiceSpecificController@store']);

	Route::get('/{id}/edit-specific/{specific_id}', 			['as' => 'edit-specific', 			'uses' => 'ServiceSpecificController@edit']);
	Route::post('/{id}/update-specific/{specific_id}', 				['as' => 'update-specific', 			'uses' => 'ServiceSpecificController@update']);

	Route::delete('/{id}/trash-specific/{specific_id}', 			['as' => 'trash-specific', 			'uses' => 'ServiceSpecificController@trash']);
	Route::post('/{id}/order-specific', 			['as' => 'order-specific', 			'uses' => 'ServiceSpecificController@order']);
	Route::post('{id}/status-specific', 			['as' => 'update-status-specific', 	'uses' => 'ServiceSpecificController@updateStatus']);
});	