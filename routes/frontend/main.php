<?php
Route::get('/home', 						[ 'as' => 'home',						'uses' => 'HomeController@index']);
Route::get('/contact', 						[ 'as' => 'contact',				    'uses' => 'ContactController@index']);
Route::get('/activities', 					[ 'as' => 'activities',				    'uses' => 'ActivitiesController@index']);
Route::get('/study-abroad', 				[ 'as' => 'study-abroad',				'uses' => 'StudyAbroadController@index']);
Route::get('/director-speech', 				[ 'as' => 'director-speech',			'uses' => 'DirectorspeechController@index']);
Route::get('/management', 			     	[ 'as' => 'management',			        'uses' => 'ManagementController@index']);
Route::get('/our-client', 			     	[ 'as' => 'our-client',			        'uses' => 'OurclientController@index']);
Route::get('/visa-services', 			    [ 'as' => 'visa-services',			    'uses' => 'VisaServeController@index']);
Route::get('/bussiness-solution', 			[ 'as' => 'bussiness-solution',			'uses' => 'BussinessController@index']);
Route::get('/education-advise', 			[ 'as' => 'education-advise',			'uses' => 'EducationController@index']);
Route::put('/sendmessage', 					[ 'as' => 'sendmessage',			    'uses' => 'ContactController@sendMassage']);






