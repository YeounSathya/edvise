<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Session;
use Validator;
use Telegram\Bot\Laravel\Facades\Telegram;

use App\Model\ContactAddress;
use App\Model\HumanResource;
use App\Model\WorkingHour;
use App\Model\Massage as Message;

class ContactController extends FrontendController
{
    
    public function index() {
        return view('frontend.contact', []);
    }

    public function sendMassage(Request $request){

        $data = array(
            'name'      =>      $request->input('name'),
            'phone'     =>      $request->input('phone'),
            'email'     =>      $request->input('email'),
            'company'   =>      $request->input('company'),
            'massage'   =>      $request->input('massage') 
        );

        Session::flash('invalidData', $data );
        Validator::make(
            $request->all(), 
            [
                'name' => 'required|min:3|max:30',
                'company' => 'required',
                'email' => 'email',
                'massage' => 'required|max:255',
                //'g-recaptcha-response' => 'required',
            ], 

            [
                
            ])->validate();

        
        $id = Message::insertGetId($data);
        
        $text = "<b>Customer Contact </b>\n"
        . "<b>Name: </b> $request->name \n"
        . "<b>Phone: </b> $request->phone \n"
        . "<b>Email Address: </b> $request->email \n"
        . "<b>Company: </b> $request->company \n"
        . "<b>Message: </b> $request->message";

        Telegram::sendMessage([
            'chat_id' => env('TELEGRAM_CHANNEL_ID', ''),
            'text' => $text,
            'parse_mode' => 'HTML'
        ]);

        Session::flash('msg', 'Your request has been sent! We will respone you soon.' );

        return redirect(route('contact','#send-contact'));
    }
}
