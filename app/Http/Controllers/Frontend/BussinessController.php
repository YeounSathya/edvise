<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Slide as Slide;
use App\Model\Partner as Partner;


class BussinessController extends FrontendController
{
    
   
    public function index() {

    		$defaultData = $this->defaultData();
    	$partner = Partner::select('id','image','contents','title')->get();

        return view ('frontend.services.bussiness-solution',['defaultData'=>$defaultData,'partner'=>$partner]);

    }
}

