<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Product;
use App\Model\Slide as Slide;
use App\Model\Partner as Partner;
use App\Model\Management as Management;
use App\Model\Bannerfooter as Bannerfooter;
use App\Model\Aboutbanner as Aboutbanner;


class ManagementController extends FrontendController
{
    
   
    public function index() {
    	$partner = Partner::select('id','image','contents','title')->get();
    	$management = Management::select('id','image','contents','title','name')->get();
    	$bannerfooter = Bannerfooter::select('id','image','contents','title')->get();
    	$aboutbanner = Aboutbanner::select('id','image','contents','title')->get();

        return view ('frontend.about.management',['partner'=>$partner,'management'=> $management, 'bannerfooter' => $bannerfooter,'aboutbanner'=>$aboutbanner]);

    }
}
