<?php

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;


use App\Model\Slide as Slide;
use App\Model\Constulting as Constulting;
use App\Model\Ourmanagement as Ourmanagement;
use App\Model\Ourupdate as Ourupdate;
use App\Model\Partner as Partner;
use App\Model\Whoweare as Whoweare;
use App\Model\Bannerfooter as Bannerfooter;





class HomeController extends FrontendController
{
    
   
    public function index() {
    	$defaultData = $this->defaultData();
    	$whoweare = Whoweare::select('id','image','contents','title')->get();
    	$partner = Partner::select('id','image','contents','title')->get();
    	$constulting = Constulting::select('id','image','contents','title')->get();
    	$ourmanagement = Ourmanagement::select('id','image','contents','title')->get();
    	$ourupdate= Ourupdate::select('id','image','title','contents','created_at')->get();
    	$bannerfooter = Bannerfooter::select('id','image','contents','title')->get();
        return view ('frontend.home',['defaultData'=>$defaultData,'constulting'=>$constulting,'ourmanagement'=>$ourmanagement,'ourupdate'=>$ourupdate,'partner' =>$partner,'whoweare' =>$whoweare,'bannerfooter' => $bannerfooter,]);

    }

}