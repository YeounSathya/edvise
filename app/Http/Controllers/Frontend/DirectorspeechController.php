<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Product;
use App\Model\Slide as Slide;
use App\Model\Director as Director;
use App\Model\Partner as Partner;
use App\Model\Bannerfooter as Bannerfooter;
use App\Model\Aboutbanner as Aboutbanner;


class DirectorspeechController extends FrontendController
{
    
   
    public function index() {
    	$defaultData = $this->defaultData();
    	$director = Director::select('id','image','title','contents')->get();
    	$partner = Partner::select('id','image','title')->get();
    	$bannerfooter = Bannerfooter::select('id','image','title','contents')->get();
    	$aboutbanner = aboutbanner::select('id','image','title','contents','name')->get();
        return view ('frontend.about.director-speech',['defaultData'=>$defaultData,'bannerfooter'=>$bannerfooter,'director'=>$director,'partner'=>$partner,'aboutbanner' => $aboutbanner]); 

    }

}