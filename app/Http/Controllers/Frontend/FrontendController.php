<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

use App\Model\Slide as Slide;
use App\Model\Partner as Partner;

class FrontendController extends Controller
{
  	
	public $defaultData = array();
    public function __construct(){
      
    }

    public function defaultData($locale = "en"){


    	App::setLocale($locale);

        //Current Language
        $parameters = Route::getCurrentRoute()->parameters();

        $this->defaultData['slides'] =  Slide::select('id','image','title')->get();

        return $this->defaultData;
    }
    
}
