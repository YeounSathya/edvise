<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

use App\Model\Product;
use App\Model\Slide as Slide;
use App\Model\Partner as Partner;
use App\Model\Bannerfooter as Bannerfooter;
use App\Model\Ourclient as Ourclient;


class OurclientController extends FrontendController
{
    
   
    public function index() {
    	$defaultData = $this->defaultData();
    	$partner = Partner::select('id','image','contents','title')->get();
    	$ourclient = ourclient::select('id','image','contents','name','title')->get();
    	$bannerfooter = Bannerfooter::select('id','image','contents','title')->get();
        return view ('frontend.about.our-client',['defaultData'=>$defaultData,'partner'=>$partner, 'bannerfooter' => $bannerfooter,'ourclient'=> $ourclient,'']);

    }
}
