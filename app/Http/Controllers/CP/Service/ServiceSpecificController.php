<?php

namespace App\Http\Controllers\CP\Service;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;
use App\Http\Controllers\CamCyber\GenerateSlugController as GenerateSlug;
use App\Model\ServiceSpecific as Model;


class ServiceSpecificController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "cp.service";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index($id){
        $data = Model::select('*')->where('service_id', $id);

        $limit     =   intval(isset($_GET['limit'])?$_GET['limit']:10); 
        $key       =   isset($_GET['key'])?$_GET['key']:"";
        $from=isset($_GET['from'])?$_GET['from']:"";
        $till=isset($_GET['till'])?$_GET['till']:"";
        $appends=array('limit'=>$limit);
        if( $key != "" ){
            $data = $data->where('title', 'like', '%'.$key.'%');
            $appends['key'] = $key;
        }
       
        if(FunctionController::isValidDate($from)){
            if(FunctionController::isValidDate($till)){
                $appends['from'] = $from;
                $appends['till'] = $till;

                $from .=" 00:00:00";
                $till .=" 23:59:59";

                $data = $data->whereBetween('created_at', [$from, $till]);
            }
        }
        $data= $data->orderBy('data_order','ASC')->paginate($limit);
        return view('cp.service.specific.index', ['route'=>$this->route, 'id'=>$id, 'data'=>$data,'appends'=>$appends]);
    }

    function order($id ,Request $request){
       $string = $request->input('string');
       $data = json_decode($string);
       //print_r($data); die;
        foreach($data as $row){
            Model::where('id', $row->id)->update(['data_order'=>$row->order]);
        }
       return response()->json([
          'status' => 'success',
          'msg' => 'Data has been ordered.'
      ]);
    }

    public function create($id){
        return view('cp.service.specific.create' , ['id'=>$id, 'route'=>$this->route]);
    }

    public function store($id, Request $request) {
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'service_id' =>   $id,
                    'title' =>   $request->input('title'),
                    'slug' => GenerateSlug::generateSlug('service_specific', $request->input('title')),
                    'content' => $request->input('content'),
                    'creator_id' => $user_id,
                    'created_at' => $now
                );
        
        Session::flash('invalidData', $data );
        Validator::make(
                        $data, 
                        [
                            
                           'title' => 'required',
                           'slug' => 'required',
                           'content' => 'required',
                           
                        ]);
        
        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
        $specific_id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
        return redirect(route('cp.service.edit-specific', ['id'=>$id, 'specific_id'=>$specific_id]));
    }

    public function edit($id = 0, $specific_id = 0){
        $this->validObj($specific_id);
        $data = Model::find($specific_id);
        return view('cp.service.specific.edit', ['route'=>$this->route, 'id'=>$id, 'specific_id'=>$specific_id, 'data'=>$data]);
    }


    public function update($id, $specific_id,Request $request){
        $user_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'service_id' =>   $id,
                    'title' =>   $request->input('title'), 
                    'slug' => GenerateSlug::generateSlug('service', $request->input('title')),
                    'content' => $request->input('content'),
                    'updater_id' => $user_id,
                    'updated_at' => $now
                );
        

        Validator::make(
                        $data, 
                        [
                            
                            'title' => 'required',
                            
                        ]);


        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
        Model::where('id', $specific_id)->update($data);

        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
    }

    public function trash($id, $specific_id){
        Model::where('id', $specific_id)->update(['deleter_id' => Auth::id()]);
        Model::find($specific_id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'servicehas been deleted'
        ]);
    }

    function updateStatus(Request $request){
      $id   = $request->input('id');
      $data = array('is_published' => $request->input('active'));
      Model::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Published status has been updated.'
      ]);
    }

    
}
