<?php

namespace App\Http\Controllers\CP\Ourupdate;

use Auth;
use Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\CamCyber\FileUploadController as FileUpload;
use App\Http\Controllers\CamCyber\FunctionController;

use App\Model\Ourupdate as Model;


class OurupdateController extends Controller
{
    protected $route; 
    public function __construct(){
        $this->route = "cp.ourupdate";
    }
    function validObj($id=0){
        $data = Model::find($id);
        if(empty($data)){
           echo "Invalide Object"; die;
        }
    }

    public function index(){
        $data = Model::select('*')->orderBy('data_order','ASC')->get();
        
        return view($this->route.'.index', ['route'=>$this->route, 'data'=>$data]);
    }
    function order(Request $request){
       $string = $request->input('string');
       $data = json_decode($string);
       //print_r($data); die;
        foreach($data as $row){
            Model::where('id', $row->id)->update(['data_order'=>$row->order]);
        }
       return response()->json([
          'status' => 'success',
          'msg' => 'Data has been ordered.'
      ]);
    }
    
    public function create(){
        return view($this->route.'.create' , ['route'=>$this->route]);
    }
    public function store(Request $request) {
        $ourupdate_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'title' =>   $request->input('title'),
                    'contents' =>   $request->input('contents'),
                    'creator_id' => $ourupdate_id,
                    'created_at' => $now
                );
        
        Session::flash('invalidData', $data );
        Validator::make(
                        $data, 
                        [
                            
                           'title' => 'required',
                           'contents' => 'contents',
                           'image' => [
                                            'mimes:jpeg,png,jpg',
                            ],
                        ]);
        $image = FileUpload::uploadFile($request, 'image', 'uploads/ourupdate');
        if($image != ""){
            $data['image'] = $image; 
        }
        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
		$id=Model::insertGetId($data);
        Session::flash('msg', 'Data has been Created!');
		return redirect(route($this->route.'.edit', $id));
    }

    public function edit($id = 0){
        $this->validObj($id);
        $data = Model::find($id);
        return view($this->route.'.edit', ['route'=>$this->route, 'id'=>$id, 'data'=>$data]);
    }

    public function update(Request $request){
        $id = $request->input('id');
        $ourupdate_id    = Auth::id();
        $now        = date('Y-m-d H:i:s');

        $data = array(
                    'title' =>      $request->input('title'), 
                    'contents' =>   $request->input('contents'),
                    'updater_id' => $ourupdate_id,
                    'updated_at' => $now
                );
        

        Validator::make(
        				$data, 
			        	[
                            
                            'title' => 'required',
                            'contents' => 'contents',
                            'image' => [
                                            'mimes:jpeg,png,jpg',
                            ],
						]);

        $image = FileUpload::uploadFile($request, 'image', 'uploads/ourupdate');
        if($image != ""){
            $data['image'] = $image; 
        }
        if($request->input('status')=="")
        {
            $data['is_published']=0;
        }else{
            $data['is_published']=1;
        }
        Model::where('id', $id)->update($data);
        Session::flash('msg', 'Data has been updated!' );
        return redirect()->back();
	}

    public function trash($id){
        Model::where('id', $id)->update(['deleter_id' => Auth::id()]);
        Model::find($id)->delete();
        Session::flash('msg', 'Data has been delete!' );
        return response()->json([
            'status' => 'success',
            'msg' => 'ourupdate has been deleted'
        ]);
    }
    function updateStatus(Request $request){
      $id   = $request->input('id');
      $data = array('is_published' => $request->input('active'));
      Model::where('id', $id)->update($data);
      return response()->json([
          'status' => 'success',
          'msg' => 'Published status has been updated.'
      ]);
    }
}
