<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
   
    protected $table = 'service'; 

    public function servicesSpecific(){

        return $this->hasMany('App\Model\ServiceSpecific','service_id');//1->many
    }


   
}
