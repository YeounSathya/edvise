@extends('frontend.layouts.master')

@section('title', 'Welcome to Edvise')
@section('active-Our Clients Say', 'current')


@section ('content')
<body class="site-body style-v1">
	<!-- Header --> 
	<header class="site-header header-s1 is-transparent is-sticky">
		<!-- Topbar -->
		<div class="topbar">
			<div class="container">
				<div class="row">
					<div class="top-aside top-left">
<!-- 						<ul class="top-nav">
							<li><a class="language" href="#">ខ្មែរ</a></li>
							<li><a class="language" href="#">EN</a></li>
						</ul> -->
					</div>
					<div class="top-aside top-right clearfix">
						<ul class="top-contact clearfix">
							<li class="t-email t-email1">
								<em class="fa fa-envelope-o" aria-hidden="true"></em>
								<span><a href="#">info@edvise.asia</a></span>
							</li>
							<li class="t-phone t-phone1">
								<em class="fa fa-phone" aria-hidden="true"></em>
								<span>+(855) 16 949 294</span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- #end Topbar -->
		<!-- Navbar -->
		<div class="navbar navbar-primary">
			<div class="container">
				<!-- Logo -->
					<a class="navbar-brand" href="index.html">
					<img class="logo logo-dark" alt="" src="{{ asset('public/frontend/assets/camcyber/logo.png') }}" srcset="{{ asset('public/frontend/assets/camcyber/logo.png') }}">
					<img class="logo logo-light" alt="" src="{{ asset('public/frontend/assets/camcyber/logo-light.png') }}" srcset="{{ asset('public/frontend/assets/camcyber/logo-light.png') }}">
				</a>
				<!-- #end Logo -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainnav" aria-expanded="false">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Q-Button for Mobile -->
					<div class="quote-btn"><a class="btn" href="get-a-quote.html">Sign in</a></div>
				</div>
				<!-- MainNav -->
				<nav class="navbar-collapse collapse" id="mainnav">
					<ul class="nav navbar-nav">
						<li class="dropdown active"><a href="{{ route('home') }}">Home</a>
						</li>
						<li class="dropdown"><a href="#">About us <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{ route('director-speech')}}">Director Speech's</a></li>
								<li><a href="{{ route('management')}}">Management Team</a></li>
								<li><a href="{{ route('our-client')}}">Our Clients Say</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="service.html" class="dropdown-toggle">Services<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{ route('study-abroad') }}">Study Abroad</a></li>
								<li><a href="#">Visa Servce</a></li>
								<li><a href="#">Education advise</a></li>
								<li><a href="#">Business Solution</a></li>
							</ul>
						</li>
						<li><a href="{{ route('activities') }}">Activities</a></li>
						<li class="quote-btn"><a class="btn" href="{{ route('contact') }}">Contact Us</a></li>
					</ul>
				</nav>     
				<!-- #end MainNav -->
			</div>
		</div>
		<!-- #end Navbar -->
		<!-- Banner/Slider -->
	@include('frontend.layouts.banner')
	</header>
	<!-- End Header -->

	<!-- Content -->
	<div class="section section-contents section-pad">
		<div class="container">
			<div class="row">

				<div class="wide-md">
					<div class="testimonials testimonials-list">
						<!-- <div class="quotes">
							<div class="quotes-text">
								<p><i>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</i></p>
								<p><i>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</i></p>
							</div>
							<div class="profile">
								<h5>John Doe</h5>
								<h6>CEO, Company Name</h6>
							</div>
						</div> -->
						<!-- <div class="quotes">
							<div class="quotes-text">
								<p><i>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</i></p>
								<p><i>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</i></p>
							</div>
							<div class="profile">
								<h5>John Doe</h5>
								<h6>CEO, Company Name</h6>
							</div>
						</div> -->
						<!-- <div class="quotes">
							<div class="quotes-text">
								<p><i>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</i></p>
								<p><i>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</i></p>
							</div>
							<div class="profile">
								<h5>Michel Walton</h5>
								<h6>CEO, Company Name</h6>
							</div>
						</div> -->
						<!-- <div class="quotes">
							<div class="quotes-text">
								<p><i>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</i></p>
								<p><i>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</i></p>
							</div>
							<div class="profile">
								<h5>Rojer Murr</h5>
								<h6>CEO, Company Name</h6>
							</div>
						</div>
					</div> -->
					@foreach( $ourclient as $row)
					<div class="quotes">
							<div class="quotes-text">
								<p><i>{{ $row->title }}<!-- Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam. --></i></p>
								<p><i>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam.</i></p>
							</div>
							<div class="profile">
								<h5>{{ $row->contents }}</h5>
								<h6>{{ $row->name }}</h6>
							</div>
					</div>
					@endforeach
				</div>

			</div>
		</div>
	</div>
	<!-- End Content -->	
	<!-- Client logo -->
	<div class="section section-logos section-pad-sm bg-light bdr-top">
		<div class="container">
			<div class="content row">

				
				<div class="owl-carousel loop logo-carousel style-v2">
					@foreach( $partner as $row)
					<div class="logo-item"><img alt="" width="190" height="82" src="{{ asset( $row ->image) }} "></div>
					<!-- <div class="logo-item"><img alt="" width="190" height="82" src="{{ asset('public/frontend/assets/image/cl-logo1-w.png') }} "></div>
					<div class="logo-item"><img alt="" width="190" height="82" src="{{ asset('public/frontend/assets/image/cl-logo2-w.png') }} "></div>
					<div class="logo-item"><img alt="" width="190" height="82" src="{{ asset('public/frontend/assets/image/cl-logo3-w.png') }} "></div>
					<div class="logo-item"><img alt="" width="190" height="82" src="{{ asset('public/frontend/assets/image/cl-logo4-w.png') }} "></div>
					<div class="logo-item"><img alt="" width="190" height="82" src="{{ asset('public/frontend/assets/image/cl-logo5-w.png') }} "></div>
					<div class="logo-item"><img alt="" width="190" height="82" src="{{ asset('public/frontend/assets/image/cl-logo6-w.png') }} "></div> -->
					@endforeach
				</div>

			</div>
		</div>	
	</div>
	<!-- End Section -->

	<!-- Call Action -->
	@foreach( $bannerfooter as $row)
	<div class="call-action cta-small has-bg bg-primary" style="background-image: url('{{ asset( $row->image) }}');">
		<div class="cta-block">
			<div class="container">
				<div class="content row">

					<div class="cta-sameline">
						<h2>{{ $row->title}}</h2>
						<p>{{$row->contents}}</p>
						<a class="btn btn-alt" href="{{ route('contact') }}">Contact Us</a>
					</div>

				</div>
			</div>
		</div>
	</div>
	@endforeach

	<!-- End Section -->
	@endsection