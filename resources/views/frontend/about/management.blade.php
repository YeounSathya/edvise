@extends('frontend.layouts.master')

@section('title', 'Welcome to Edvise')
@section('active-director-speeche', 'current')


@section ('content')
<body class="site-body style-v1">
	<!-- Header --> 
	<header class="site-header header-s1 is-transparent is-sticky">
		<!-- Topbar -->
		<div class="topbar">
			<div class="container">
				<div class="row">
					<div class="top-aside top-left">
					</div>
					<div class="top-aside top-right clearfix">
						<ul class="top-contact clearfix">
							<li class="t-email t-email1">
								<em class="fa fa-envelope-o" aria-hidden="true"></em>
								<span><a href="#">info@edvise.asia</a></span>
							</li>
							<li class="t-phone t-phone1">
								<em class="fa fa-phone" aria-hidden="true"></em>
								<span>+(855) 16 949 294</span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- #end Topbar -->
		<!-- Navbar -->
		<div class="navbar navbar-primary">
			<div class="container">
				<!-- Logo -->
					<a class="navbar-brand" href="index.html">
					<img class="logo logo-dark" alt="" src="{{ asset('public/frontend/assets/camcyber/logo.png') }}" srcset="{{ asset('public/frontend/assets/camcyber/logo.png') }}">
					<img class="logo logo-light" alt="" src="{{ asset('public/frontend/assets/camcyber/logo-light.png') }}" srcset="{{ asset('public/frontend/assets/camcyber/logo-light.png') }}">
				</a>
				<!-- #end Logo -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainnav" aria-expanded="false">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Q-Button for Mobile -->
					<div class="quote-btn"><a class="btn" href="get-a-quote.html">Sign in</a></div>
				</div>
				<!-- MainNav -->
				<nav class="navbar-collapse collapse" id="mainnav">
					<ul class="nav navbar-nav">
						<li class="dropdown active"><a href="{{ route('home') }}">Home</a>
						</li>
						<li class="dropdown"><a href="#">About us <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{ route('director-speech')}}">Director Speech's</a></li>
								<li><a href="{{ route('management')}}">Management Team</a></li>
								<li><a href="{{ route('our-client')}}">Our Clients Say</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="service.html" class="dropdown-toggle">Services<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{ route('study-abroad') }}">Study Abroad</a></li>
								<li><a href="{{ route('visa-services')}}">Visa Servce</a></li>
								<li><a href="{{ route('education-advise')}}">Education advise</a></li>
								<li><a href="{{ route('bussiness-solution')}}">Business Solution</a></li>
							</ul>
						</li>
						<li><a href="{{ route('activities') }}">Activities</a></li>
						<li class="quote-btn"><a class="btn" href="{{ route('contact') }}">Contact Us</a></li>
					</ul>
				</nav>     
				<!-- #end MainNav -->
			</div>
		</div>

		<!-- #end Navbar -->
		<!-- Banner/Static -->
		@foreach($aboutbanner as $row)
		<div class="banner banner-static">
			<div class="banner-cpn">
				<div class="container">
					<div class="content row">
					
						<div class="banner-text">
							<!-- <h1 class="page-title">Manangement Team</h1> -->
							<h1 class="page-title">{{ $row->title }}</h1>
							<!-- <p>Nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat do eiusmod tempor incidid.</p> -->	
							<p>{{ $row->contents}}</p>
							<p>{{ $row->name}}</p>					
						</div>
						<div class="page-breadcrumb">
							<ul class="breadcrumb">
								<a href="{{ route('home') }}">Home</a>
								<li><a href="$">About Us</a></li>
								<li class="active"><span>Our Team</span></li>
							</ul>
						</div>
						
					</div>
				</div>
			</div>
			<div class="banner-bg imagebg">
				<img src="{{ asset( $row->image ) }}" alt="" />
			</div>
		</div>
		@endforeach
		<!-- #end Banner/Static -->
	</header>
	<!-- End Header -->
	<!-- Content Section -->
	<div class="section section-contents section-pad">
		<div class="container">
			<div class="content row">
			
				<div class="row">
					<div class="col-md-8">
					
						@foreach($management as $row)
						<div class="team-profile">
							<div class="team-member row">								
								<div class="team-photo col-md-4 col-sm-5 col-xs-12">
									<img alt="" src="{{ asset( $row->image ) }}">
								</div>
								<div class="team-info col-md-8 col-sm-7 col-xs-12">
									<h3 class="name">{{$row->name}}</h3>
									<p class="sub-title">{{ $row->title}}</p>
									<p>{{$row->contents}}</p>
								</div>
							</div>
						</div>
						@endforeach

						<!-- <div class="team-profile">
							<div class="team-member row">								
								<div class="team-photo col-md-4 col-sm-5 col-xs-12">
									<img alt="" src="{{ asset('public/frontend/assets/image/team/team-a.jpg') }}">
								</div>
								<div class="team-info col-md-8 col-sm-7 col-xs-12">
									<h3 class="name">Kunthel OUM</h3>
									<p class="sub-title">- Managing Director</p>
									<p>Kunthel is passionate in social services and he is a young social entrepreneur with
									extensive experience in visa consultancy, education advisory, and new started-up in
									digital biz, education and hospitality. He has started his consultancy service since 2003
									and officially registered as Edvise. Prior to this, he had experiences in market analysis,
									feasibility studies, due diligent and investment analysis. He has jointed private equity firm
									as a business consultant for 3 years. Kunthel holds a Master in international MBA since
									2009 and has a bachelor of hospitality management in 2005.</p>
								</div>
							</div>
						</div> -->
						<!-- <div class="team-profile">
							<div class="team-member row">
								<div class="team-photo col-md-4 col-sm-5 col-xs-12">
									<img alt="" src="{{ asset('public/frontend/assets/image/team/team-b.jpg') }}">
								</div>
								<div class="team-info col-md-8 col-sm-7 col-xs-12">
									<h3 class="name">Dolla PHA</h3>
									<p class="sub-title">- Education Dept Manager</p>
									<p>In regards to matters related to studying abroad and documents preparations, Dolla has
									been a responsible consultant for those particular information. He has years of
									experiences in these fields since he was also once a Managing assistant of the Student
									Recruitment Department. His skills in translation is professional due to his past
									involvements with English teaching for more than 10 years. He has a Master degree in
									Business Administration and has been to undeniably various and useful workshops.</p>

								</div>
							</div>
						</div>
						<div class="team-profile">
							<div class="team-member row">
								<div class="team-photo col-md-4 col-sm-5 col-xs-12">
									<img alt="" src="{{ asset('public/frontend/assets/image/team/team-c.jpg') }}">
								</div>
								<div class="team-info col-md-8 col-sm-7 col-xs-12">
									<h3 class="name">Eiza CHHUN</h3>
									<p class="sub-title">- Visa Service Dept Manager</p>
									<p>Eiza is managing the whole visa department that strongly influences both Cambodians
									and foreigners as he assists in the documents in regards to visa applications since 2016.
									His work experiences started in 2010 when he began working as a senior consultant and
									a translator. Furthermore, he has experiences in training the staff and he is punctual on
									meetings. He had earned his Master’s degree in Business Administration in 2018. He was
									rewarded with a scholarship and had earned a degree in English Literature in 2013.</p>

								</div>
							</div>
						</div>
						<div class="team-profile">
							<div class="team-member row">
								<div class="team-photo col-md-4 col-sm-5 col-xs-12">
									<img alt="" src="{{ asset('public/frontend/assets/image/team/team-d.jpg') }}">
								</div>
								<div class="team-info col-md-8 col-sm-7 col-xs-12">
									<h3 class="name">Molyka Brasoeur</h3>
									<p class="sub-title">- Education Edviser</p>
									<p>Kunthel is passionate in social services and he is a young social entrepreneur with extensive experience in visa consultancy, education advisory, and new started-up in digital biz, education and hospitality. He has started his consultancy service since 2003 and officially registered as Edvise. Prior to this, he had experiences in market analysis, feasibility studies, due diligent and investment analysis. He has jointed private equity firm as a business consultant for 3 years. Kunthel holds a Master in international MBA since 2009 and has a bachelor of hospitality management in 2005.</p>

								</div>
							</div>
						</div> -->
						
					</div>

					<!-- Sidebar -->
					<div class="col-md-4">
						<div class="sidebar-right">

							<div class="wgs-box wgs-menus">
								<div class="wgs-content">
									<ul class="list list-grouped">
										<li class="list-heading">
											<span>Our Services</span>
											<ul>
												<li><a href="{{ route('study-abroad') }}">Study Abroad</a></li>
												<li><a href="{{ route('visa-services')}}">Visa Servce</a></li>
												<li><a href="{{ route('education-advise')}}">Education advise</a></li>
												<li><a href="{{ route('bussiness-solution')}}">Business Solution</a></li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
							
						</div>
					</div>
					<!-- Sidebar #end -->
				</div>
				
			</div>
		</div>		
	</div>
	<!-- End Section -->
	<!-- Client logo -->
	<div class="section section-logos section-pad-sm bg-light bdr-top">
		<div class="container">
			<div class="content row">

				
				<div class="owl-carousel loop logo-carousel style-v2">
					@foreach( $partner as $row)
					<div class="logo-item"><img alt="" width="190" height="82" src="{{ asset( $row ->image) }} "></div>
					<!-- <div class="logo-item"><img alt="" width="190" height="82" src="{{ asset('public/frontend/assets/image/cl-logo1-w.png') }} "></div>
					<div class="logo-item"><img alt="" width="190" height="82" src="{{ asset('public/frontend/assets/image/cl-logo2-w.png') }} "></div>
					<div class="logo-item"><img alt="" width="190" height="82" src="{{ asset('public/frontend/assets/image/cl-logo3-w.png') }} "></div>
					<div class="logo-item"><img alt="" width="190" height="82" src="{{ asset('public/frontend/assets/image/cl-logo4-w.png') }} "></div>
					<div class="logo-item"><img alt="" width="190" height="82" src="{{ asset('public/frontend/assets/image/cl-logo5-w.png') }} "></div>
					<div class="logo-item"><img alt="" width="190" height="82" src="{{ asset('public/frontend/assets/image/cl-logo6-w.png') }} "></div> -->
					@endforeach
				</div>

			</div>
		</div>	
	</div>
	<!-- End Section -->

	<!-- Call Action -->
	@foreach( $bannerfooter as $row)
	<div class="call-action cta-small has-bg bg-primary" style="background-image: url('{{ asset( $row->image) }}');">
		<div class="cta-block">
			<div class="container">
				<div class="content row">

					<div class="cta-sameline">
						<h2>{{ $row->title}}</h2>
						<p>{{$row->contents}}</p>
						<a class="btn btn-alt" href="{{ route('contact') }}">Contact Us</a>
					</div>

				</div>
			</div>
		</div>
	</div>
	@endforeach

	<!-- End Section -->
	@endsection