@extends('frontend.layouts.master')

@section('title', 'Welcome to Edvise')
@section('active-director-speeche', 'current')


@section ('content')
<body class="site-body style-v1">
	<!-- Header --> 
	<header class="site-header header-s1 is-transparent is-sticky">
		<!-- Topbar -->
		<div class="topbar">
			<div class="container">
				<div class="row">
					<div class="top-aside top-left">
					</div>
					<div class="top-aside top-right clearfix">
						<ul class="top-contact clearfix">
							<li class="t-email t-email1">
								<em class="fa fa-envelope-o" aria-hidden="true"></em>
								<span><a href="#">info@edvise.asia</a></span>
							</li>
							<li class="t-phone t-phone1">
								<em class="fa fa-phone" aria-hidden="true"></em>
								<span>+(855) 16 949 294</span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- #end Topbar -->
		<!-- Navbar -->
		<div class="navbar navbar-primary">
			<div class="container">
				<!-- Logo -->
					<a class="navbar-brand" href="index.html">
					<img class="logo logo-dark" alt="" src="{{ asset('public/frontend/assets/camcyber/logo.png') }}" srcset="{{ asset('public/frontend/assets/camcyber/logo.png') }}">
					<img class="logo logo-light" alt="" src="{{ asset('public/frontend/assets/camcyber/logo-light.png') }}" srcset="{{ asset('public/frontend/assets/camcyber/logo-light.png') }}">
				</a>
				<!-- #end Logo -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainnav" aria-expanded="false">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Q-Button for Mobile -->
					<div class="quote-btn"><a class="btn" href="get-a-quote.html">Sign in</a></div>
				</div>
				<!-- MainNav -->
				<nav class="navbar-collapse collapse" id="mainnav">
					<ul class="nav navbar-nav">
						<li class="dropdown active"><a href="{{ route('home') }}">Home</a>
						</li>
						<li class="dropdown"><a href="#">About us <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{ route('director-speech')}}">Director Speech's</a></li>
								<li><a href="{{ route('management')}}">Management Team</a></li>
								<li><a href="{{ route('our-client')}}">Our Clients Say</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="service.html" class="dropdown-toggle">Services<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{ route('study-abroad') }}">Study Abroad</a></li>
								<li><a href="{{ route('visa-services')}}">Visa Servce</a></li>
								<li><a href="{{ route('education-advise')}}">Education advise</a></li>
								<li><a href="{{ route('bussiness-solution')}}">Business Solution</a></li>
							</ul>
						</li>
						<li><a href="{{ route('activities') }}">Activities</a></li>
						<li class="quote-btn"><a class="btn" href="{{ route('contact') }}">Contact Us</a></li>
					</ul>
				</nav>     
				<!-- #end MainNav -->
			</div>
		</div>

		<!-- #end Navbar -->
		<!-- Banner/Static -->
		@foreach( $aboutbanner as $row )
		<div class="banner banner-static">
			<div class="banner-cpn">
				<div class="container">
					<div class="content row">
					
						<div class="banner-text">
							<!-- <h1 class="page-title">Manangement Team</h1> -->
							<h1 class="page-title"><!-- {{ $row->title }} -->Portfolio</h1>
							<!-- <p>Nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat do eiusmod tempor incidid.</p> -->	
							<p><!-- {{ $row->contents}} -->Nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat do eiusmod tempor incidid.</p>					
						</div>
						<div class="page-breadcrumb">
							<ul class="breadcrumb">
								<a href="{{ route('home') }}">Home</a>
								<li><a href="$">About Us</a></li>
								<li class="active"><span>Our Team</span></li>
							</ul>
						</div>
						
					</div>
				</div>
			</div>
			<div class="banner-bg imagebg">
				<img src="{{ asset( $row->image ) }}" alt="" />
			</div>
		</div>
		@endforeach
		<!-- #end Banner/Static -->
	</header>	<!-- End Header -->
	<!-- Content Section -->
	<div class="section section-contents section-pad">
		<div class="container">
			@foreach ( $director as $row)
			<div class="content row">
			

				<div class="portfolio-details row">
					<div class="col-sm-6 res-m-bttm">

						<h2> {{ $row->title}} </h2>
						<p> <!-- {{ $row->contents }} --> </p>
						<p>Edvise has been in the visa services and education counseling industry for more than a
						decade with highly successful rates. We have gained trusts from both partners and clients
						which is the top priority in our business.</p>
						<p>We provide counseling, guidance and assistance as much as we could to make sure our
						clients choose the right choice in order to respond to their needs and to achieve their
						goals.</p>
						<p>Our people are at the absolute heart of providing the highest quality of services to our
						clients regardless of any circumstances. We strive to ensure that our clients are well
						treated with our professional and ethical standards of working when they choose our
						services. With our knowledge, expertise and ability to put ourselves in your shoes, we
						strongly believe that we are able to critically analyze the cases and suggest the most
						effective and efficient solutions your issues.</p>
						<p>We are looking forward to consolidating more people around the world and helping them
						in realizing their dreams. This is our ambitious, yet meaningful mission in life.</p>

					</div>

					<div class="col-sm-6">
						<img src="{{ asset( $row->image) }}" alt="">
						<div class="gaps size-md"></div>
					</div>
				</div>
	       				
			</div>
			@endforeach
		</div>		
	</div>
	<!-- End Section -->
	<!-- Client logo -->
	
	<div class="section section-logos section-pad-sm bg-light bdr-top">
		<div class="container">
			<div class="content row">

				<div class="owl-carousel loop logo-carousel style-v2">
					@foreach( $partner as $row)
					<div class="logo-item"><img alt="" width="190" height="82" src="{{ asset( $row ->image) }} "></div>
					
					@endforeach
				</div>

			</div>
		</div>	
	</div>
	<!-- End Section -->

	<!-- Call Action -->
	@foreach( $bannerfooter as $row)
	<div class="call-action cta-small has-bg bg-primary" style="background-image: url('{{ asset( $row->image) }}');">
		<div class="cta-block">
			<div class="container">
				<div class="content row">

					<div class="cta-sameline">
						<h2>{{ $row->title}}</h2>
						<p>{{$row->contents}}</p>
						<a class="btn btn-alt" href="{{ route('contact') }}">Contact Us</a>
					</div>

				</div>
			</div>
		</div>
	</div>
	@endforeach

	
@endsection