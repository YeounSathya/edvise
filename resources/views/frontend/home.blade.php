@extends('frontend.layouts.master') @section('title', 'Welcome to Edvise') @section('active-home', 'current') @section ('content')
<!-- =========================slide -->
@include('frontend.layouts.banner')

<!-- ==========================endslide -->
<!-- Service Section -->
<div class="section section-services">
    <div class="container">
        <div class="content row">
            <!-- Feature Row  -->
            <div class="feature-row feature-service-row row feature-s4 off-text boxed-filled boxed-w">
                <div class="heading-box clearfix">
                    <div class="col-sm-3">
                        <h2 class="heading-section">Constulting Expert</h2>
                    </div>
                    <div class="col-sm-8 col-sm-offset-1">
                        <span>Years of knowledge, along with care and attention brings with us the greatest results for our clients.</span>
                    </div>
                </div>
                @foreach ( $constulting as $row)
                <div class="col-md-3 col-sm-6 col-xs-6 odd">
                    <div class="feature">
                        <a href="#">
                            <div class="fbox-photo">
                                <img src="{{ asset( $row->image) }}" alt="">
                            </div>
                            <div class="fbox-over">
                                <h3 class="title"> {{  $row->title }} </h3>
                                <div class="fbox-content">
                                    <p>{{ $row->contents }} </p>
                                    <span class="btn">Learn More</span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
            <!-- Feture Row  #end -->

        </div>
    </div>
</div>
<!-- End Section -->

<!-- Content -->

<div class="section section-content section-pad">
    @foreach( $whoweare as $row)
    <div class="container">
        <div class="content row">

            <div class="row row-vm">
                <div class="col-md-6 res-m-bttm">
                    <h5 class="heading-sm-lead">About us</h5>
                    <h2 class="heading-section">{{ $row->title }}</h2>
                    <p>{{ $row->contents }}</p>
                </div>
                <div class="col-md-5 col-md-offset-1">
                    <img class="no-round" src="{{ asset ($row->image) }}" alt="">
                </div>
            </div>

        </div>
    </div>
    @endforeach
</div>

<div class="section section-contents section-pad image-on-right bg-light">
    <div class="container">
        <div class="row">

            <h5 class="heading-sm-lead">Our Services</h5>
            <h2 class="heading-section">What we do</h2>
            <div class="feature-intro">
                <div class="row">
                    <div class="col-sm-7 col-md-6">
                        <div class="row">
                            <div class="col-sm-6 res-s-bttm">
                                <div class="icon-box style-s1 photo-plx-full">
                                    <em class="fa fa-bar-chart-o" aria-hidden="true"></em>
                                </div>
                                <h4>Study Abroad</h4>
                                <p>The advantages of overseas education are very high in numbers nowadays, since it facilitates better exposures in wider horizons. Obtaining a globally accepted degree from abroad allows people to travel and live anywhere in the world with highflying careers, a truly cosmopolitan experience.</p>
                            </div>
                            <div class="col-sm-6">
                                <div class="icon-box style-s1">
                                    <em class="fa fa-users" aria-hidden="true"></em>
                                </div>
                                <h4>Visa Servce</h4>
                                <p>Our visa service were established for various reasons such as to reunite the family members, to give a chance to Cambodians to settle down with their family in the same territory filled with conveniences.</p>
                            </div>
                            <div class="gaps size-lg"></div>
                            <div class="col-sm-6 res-s-bttm">
                                <div class="icon-box style-s1">
                                    <em class="fa fa-credit-card" aria-hidden="true"></em>
                                </div>
                                <h4>Education advise</h4>
                                <p>Edvise Academy provides high quality English language training to both young people and adults to help them to achieve their English language goals.</p>
                            </div>
                            <div class="col-sm-6">
                                <div class="icon-box style-s1">
                                    <em class="fa fa-trademark" aria-hidden="true"></em>
                                </div>
                                <h4>Business Solution</h4>
                                <p>We help our customers implement today's mission as well as adjust to and incorporate new technologies to make sure that they can deliver into the future.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
    <div class="section-bg imagebg"><img src="{{ asset('public/frontend/assets/image/photo-half-a.jpg') }} " alt=""></div>
</div>
<!-- End Content -->

<!-- Content -->
<div class="section section-contents section-pad has-bg fixed-bg light bg-alternet">
    <div class="container">
        <div class="row">

            <div class="row">
                <div class="col-md-4 pad-r res-m-bttm">
                    <h2 class="heading-lead">Why choosing Edvise?</h2>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-sm-6 res-s-bttm">
                            <div class="icon-box style-s4 photo-plx-full">
                                <em class="fa fa-bar-chart-o" aria-hidden="true"></em>
                            </div>
                            <h4>Experienced</h4>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan.</p>
                        </div>
                        <div class="col-sm-6">
                            <div class="icon-box style-s4">
                                <em class="fa fa-users" aria-hidden="true"></em>
                            </div>
                            <h4>Vibrant</h4>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan.</p>
                        </div>
                        <div class="gaps size-lg"></div>
                        <div class="col-sm-6 res-s-bttm">
                            <div class="icon-box style-s4">
                                <em class="fa fa-credit-card" aria-hidden="true"></em>
                            </div>
                            <h4>Professional</h4>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan.</p>
                        </div>
                        <div class="col-sm-6">
                            <div class="icon-box style-s4">
                                <em class="fa fa-trademark" aria-hidden="true"></em>
                            </div>
                            <h4>Trademarks</h4>
                            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusan.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-bg imagebg"><img src="{{ asset('public/frontend/assets/image/plx-full.jpg') }} " alt=""></div>
</div>
<!-- End Content -->
<!-- Teams -->
<div class="section section-teams section-pad bdr-bottom">
    <div class="container">
        <div class="content row">

            <div class="col-md-offset-2 col-md-8 center">
                <h5 class="heading-sm-lead">The Team</h5>
                <h2 class="heading-section">Our Management Team</h2>
            </div>
            <div class="gaps"></div>
            
            <div class="team-member-row row">
            	@foreach( $ourmanagement as $row)
                <div class="col-md-3 col-sm-6 col-xs-6 even">
                    <!-- Team Profile -->
                    <div class="team-member">
                        <div class="team-photo">
                            <img alt="" src="{{ asset ($row->image) }} ">
                        </div>
                        <div class="team-info center">
                            <h4 class="name">{{ $row->title }}</h4>
                            <p class="sub-title">{{ $row->contents }}</p>
                        </div>
                    </div>
                    <!-- Team #end -->
                </div>
                @endforeach
            </div>

        </div>
    </div>
</div>
<!-- End Section -->
<!-- Latest News -->
<div class="section section-news section-pad">
    <div class="container">
        <div class="content row">

            <h5 class="heading-sm-lead center">Latest News</h5>
            <h2 class="heading-section center">Our Updates</h2>

            <div class="row">
                <!-- Blog Post Loop -->
                <div class="blog-posts">
                    @foreach( $ourupdate as $row )
                    <div class="post post-loop  col-md-4">
                        <div class="post-thumbs">
                            <a href="news-details.html"><img alt="" src="{{ asset( $row->image) }}"></a>
                        </div>
                        <div class="post-entry">
                            <div class="post-meta"><span class="pub-date">{{ Carbon\Carbon::parse($row->created_at)->format('d') }}-{{ Carbon\Carbon::parse($row->created_at)->format('F') }}-{{ Carbon\Carbon::parse($row->created_at)->format('Y') }}</span></div>
                            <h2><a href="news-details.html">{{ $row->title}}</a></h2>
                            <p>{{ $row->contents}}</p>
                            <a class="btn btn-alt" href="#">Read More</a>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>

        </div>
    </div>
</div>
<!-- End Section -->
<!-- Client logo -->
<div class="section section-logos section-pad-sm bg-light bdr-top">
    <div class="owl-carousel loop logo-carousel style-v2">
        @foreach( $partner as $row)
        <div class="logo-item"><img alt="" width="190" height="82" src="{{ asset( $row ->image) }} "></div>

        @endforeach
    </div>
</div>
<!-- End Section -->

<!-- Call Action -->
@foreach( $bannerfooter as $row)
<div class="call-action cta-small has-bg bg-primary" style="background-image: url('{{ asset( $row->image) }}');">
    <div class="cta-block">
        <div class="container">
            <div class="content row">

                <div class="cta-sameline">
                    <h2>{{ $row->title}}</h2>
                    <p>{{$row->contents}}</p>
                    <a class="btn btn-alt" href="{{ route('contact') }}">Contact Us</a>
                </div>

            </div>
        </div>
    </div>
</div>
@endforeach @endsection