@extends('frontend.layouts.master')

@section('title', 'Contact Us | '.env('APP_NAME'))
@section('active-contact', 'current')


@section ('content')


<!-- Content -->
	<div class="section section-contents section-contact section-pad">
		<div class="container">
			<div class="content row">

				<h2 class="heading-lg">Contact Us</h2>
				<div class="contact-content row">
					<div class="drop-message col-md-7 res-m-bttm">
						<p>Want to work with us or need more details about consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.</p>

						@if(Session::has('msg'))
						<div class="alert alert-success" role="alert">
  							{{ Session::get('msg') }}
						</div>
						@endif
						@if (count($errors) > 0)
	                    	<div class="alert alert-danger" role="alert">
	                            Sending fail! Please Try again!
	                        </div>
	                    @endif
						<form  class="form-quote" action="{{ route('sendmessage') }}" method="post">
							{{ csrf_field() }} 
							{{ method_field('PUT') }}
								<div class="form-group row">
									<div class="form-field col-md-6 form-m-bttm">
										<input name="name" type="text" placeholder="Name *" class="form-control required">
									</div>
									<div class="form-field col-md-6">
										<input name="company" type="company" placeholder="Company *" class="form-control">
									</div>
								</div>
								<div class="form-group row">
									<div class="form-field col-md-6 form-m-bttm">
										<input name="email" type="email" placeholder="Email *" class="form-control required email">
									</div>
									<div class="form-field col-md-6">
										<input name="phone" type="text" placeholder="Phone *" class="form-control required">
									</div>
								</div>
			
								<div class="form-group row">
									<div class="form-field col-md-12">
										<textarea name="massage" placeholder="Messages *" class="txtarea form-control required"></textarea>
									</div>
								</div>
								<input type="text" class="hidden" name="form-anti-honeypot" value="">
								<!-- <button type="submit" class="btn">Submit</button> -->
								<input type="submit" class="btn" placeholder="Submit">
								<div class="form-results"></div>
							</form>
					</div>
					<div class="contact-details col-md-4 col-md-offset-1">
						<ul class="contact-list">
							<li><em class="fa fa-map" aria-hidden="true"></em>
								<span>1234 Sed ut perspiciatis Road, <br>At vero eos, D58 8975, London.</span>
							</li>
							<li><em class="fa fa-phone" aria-hidden="true"></em>
								<span>Toll Free : (123) 4567 8910<br>
								Telephone : (123) 1234 5678</span>
							</li>
							<li><em class="fa fa-envelope" aria-hidden="true"></em>
								<span>Email : <a href="#">info@sitename.com</a></span>
							</li>
							<li>
								<em class="fa fa-clock-o" aria-hidden="true"></em><span>Sat - Thu: 8AM - 7PM </span>
							</li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- End Content -->
	@endsection