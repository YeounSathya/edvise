<!-- End Header -->
<!-- End Header -->	@extends('frontend.layouts.master')

@section('title', 'Welcome to Edvise')
@section('active-activities', 'current')


@section ('content')
	
	@include('frontend.layouts.banner')
	<div class="section section-contents section-pad">
		<div class="container">
			<div class="content row">
			
				<div class="row">
					<div class="col-md-8">

						<h2 class="heading-lg">Study abroad</h2>
						<p class="">The advantages of overseas education are very high in numbers nowadays, since it
							facilitates better exposures in wider horizons. Obtaining a globally accepted degree from
							abroad allows people to travel and live anywhere in the world with highflying careers, a
							truly cosmopolitan experience.</p><br>

						<p class="">Education and Visa Service (Edvise) provides overall support from pre-application to
							admission processes. Edvise offers a time-tested and cost effective service package for
							students to ensure successful admissions.</p><br>

						<p class="">Edvise has long-standing experiences in connecting aspiring students with educational
							institutions across the globe. Today, we can place students in any country from Australia
							to France for any programs from Astronomy to Zen Buddhism. Distances are no more
							barriers for aspiring students as we lead a global life through Edvise.</p><br>
						
						<h3>Admissions Information</h3>
						<p>Edvise counselors help students in admission process by advising them of required
							documentations and assisting them with the necessary application forms and evidences.
							Edvise counselors work closely with admission staff at oversea institutions and help
							resolve unusual queries for our students.
							We also help students in gaining credits for their prior equivalent educational
							qualifications. Students applying through Edvise can also avail application fee waivers
							and get spot admission offers*.</p>
	
						<h3 class="">General Admission Requirement</h3>
						<p>Documentation you’ll need to apply:</p><br>
						<p>-A completed and signed school or university application form</p>
						<p>-A certified copy of your academic results, including full academic transcripts</p>
						<p>-A certified copy of your passport’s personal details page</p>
						<p>-TOEFL score taken within the last 24 months with an overall Score of 525 / 197 /
						70 and above with the minimum validity of 6 months at the time of applying. OR</p>
						<p>-IELTS score taken within the last 24 months with an overall score of 5.5/ 6.0, with
							the minimum validity of 6 months at the time of applying. If the student doesn’t
							satisfy the above criteria for English, he will need to study ESL.</p>
						<p>-Personal Statement (Essay)</p>
						<p>-Letter of Recommendation: Students are required to submit at least one
							recommendation letter, preferably from a faculty member under who has taught
							the student in college / school.</p>
						<p>-If original documents aren’t in English, they’ll need to supply certified translations
							as well as the documents.</p>

						<h3 class="">Counselors</h3>
						<p>Edvise counselors are trained in counseling students for multiple destinations and at all
							levels of education. Edvise is one of the few agencies that sends its counselors overseas
							to be trained within institutions on programs, student experiences and lifestyles on
							campuses.</p><br>
						


						<p>A large number of our counselors and managers have teaching backgrounds and
							therefore, have an in-depth understanding of student needs and career aspirations. A
							number of our counselors and their family members have been educated overseas which
							helps them guide students through their personal experiences.
							Edvise counselors have expertise to offer you Admission counseling in - school level
							programs, diploma, undergraduate, postgraduate and doctoral level programs.</p>

						<p>Our counselors will advise you on Visa requirements and help you in every step till you
							get your visa.</p><br>
						


						<p>The following are the counseling protocols within E that every counselor must undertake;</p>



						<p>-Provide genuine unbiased counseling to students in terms of destinations,
							institutions and the program of studies.</p>

						<p>-Respects towards students and their parents regardless of their backgrounds.</p>

						<p>-The counselor must provide information to the students on all of the facets of the
							programs, lifestyles, costs and all associated current and relevant information.</p>
						<p>-Counselor must provide a customer survey form after their counseling sessions.</p>
						<p>-Counselors must provide the students the information on complaints procedures
							(available on the website) if requested.</p><br>
						<p>Counselors must provide daily reports to the Area Manager on counseled students.</p>
						<!-- End Acorrdion -->
						<img src="{{ asset('public/frontend/assets/image/photo-lg-a.jpg') }}" alt="" class="aligncenter">
						<h3 class="">Accommodation in overall</h3>
						<p>We have fully assist our students with the accommodation and provide students variety
						accommodation option to meet their budget and lifestyle.
						Pre-departure</p>
						<p>We have fully assist our students with the accommodation and provide students variety
						accommodation option to meet their budget and lifestyle.</p>
						<h3 class="">Pre-departure</h3>
						<p>Pre-departure instruction and overseas services, arrange overseas accommodation,
						airport pick-up services, and help students to get familiar with registration procedures and
						so on.</p>
					</div>
					
					<!-- Sidebar -->
					<div class="col-md-4">
						<div class="sidebar-right">

							<div class="wgs-box wgs-menus">
								<div class="wgs-content">
									<ul class="list list-grouped">
										<li class="list-heading">
											<span>Visa inquiries and Information</span>
											<ul>
												<li><a href="service-single.html">Visa for Australia</a></li>
												<li class="active"><a href="service-single-alter.html">Visa for Canada</a></li>
												<li><a href="service-single.html">Visa for New Zealand</a></li>
												<li><a href="service-single-alter.html">Visa for Singapore</a></li>
												<li><a href="service-single.html">Visa for United Kingdom</a></li>
												<li><a href="service-single-alter.html">Visa for USA</a></li>
											</ul>
										</li>
									</ul>									
								</div>
							</div>
							
							<div class="wgs-box boxed light has-bg has-bg-image">
								<div class="wgs-content">
									<h3>Innovative Tools for Investor</h3>
									<p>We employ a long-established strategy, sector-focused investing across all of our markets globally...</p>
									<a href="service-single.html" class="btn btn-alt btn-outline"> Learn More</a>
								</div>
								<div class="wgs-bg imagebg bg-image-loaded" style="background-image: url(&quot;image/photo-sd-a.jpg&quot;);">
									<img src="image/photo-sd-a.jpg" alt="">
								</div>
							</div>
							
							<div class="wgs-box boxed bg-secondary light">
								<div class="wgs-content">
									<h3>Need Help To Grow Your Business?</h3>
									<p>Investment Expert will help you start your own company.</p>
									<a href="contact.html" class="btn btn-light"> Get In Touch</a>
								</div>
							</div>

						</div>
					</div>
					<!-- Sidebar #end -->
				</div>
				
			</div>
		</div>		
	</div>
@endsection
	
