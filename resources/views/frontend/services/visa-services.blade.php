<!-- End Header -->
<!-- End Header -->	@extends('frontend.layouts.master')

@section('title', 'Welcome to Edvise')
@section('active-activities', 'current')


@section ('content')
	
	@include('frontend.layouts.banner')
	<div class="section section-contents section-pad">
		<div class="container">
			<div class="content row">
			
				<div class="row">
					<div class="col-md-8">

						<h2 class="heading-lg">Visa for Australia</h2>
						<p class="">Students traveling to Australia for studies need to apply for an appropriate student visa,
						as there are different visas that are being offered to different students. A primary or
						secondary school student’s visa is different from the visa offered to a student of vocational
						education or a student of postgraduate research. Thus, one should carefully analyze
						regarding to the category they fit in.</p><br>

						<p class="">To begin with, a student must be accepted for a full-term study in a registered course by
						a registered organization (under Commonwealth Register of Institutions and Courses for
						Overseas Students). Besides submitting the essential documents at the time of applying
						for the visa, one must fulfill the following criteria to be eligible for a student’s visa:</p><br>

						<p class="">- Sound Financial capacity (ability to pay for the air fare, tuition fee and living costs</p><br>
						<p class="">- Proficiency in English</p><br>
						<p class="">- Be of good character</p><br>
						<p class="">- Be of sound health</p><br>
						<p class="">- Have a health insurance through the Overseas Student Health Cover (UNI)</p><br>
						<p class="">- Have no pending debt to the Commonwealth of Australia or have made provisions
						to repay the same.</p><br>

						<p class="">In regards to the steps for a student visa:</p><br>

						<p class="">- Consultancies with the students in regards to the regions and universities that they
						want to attend</p><br>
						<p class="">- Check the admission requirement forms and prepare and fill out the admission
						forms, submit the admission fee and prepared other necessary documents such
						as bank settlements, passport, transcripts and etc.</p><br>
						<p class="">- Once the school approved the submission, the school will issue an enrollment
						called Confirm of Enrollment (COE)</p><br>
						<p class="">- Pay for the fee</p><br>
						<p class="">- Fill out the application form online</p><br>
						<p class="">- Submit the documents to the embassy or to a visa center and prepare any
						additional documents with a scheduled appointment</p><br>

						<h3 class="color-primary">FAQ</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Enim ad minim veniam quis nostrud exerci liquip ex modo.</p>
						<!-- Acorrdion Panels -->
						<div class="panel-group accordion" id="general" role="tablist" aria-multiselectable="true">
							<!-- each panel -->
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i1">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i1" aria-expanded="false">
											Why study in Australia?
											<span class="plus-minus"><span></span></span>
										</a>
									</h4>
								</div>
								<div id="ques-ans-i1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i1">
									<div class="panel-body">
										  <p>With a motto of Live, Learn, Grow Australia has opened its arms to the worldwide student
											community. A heaven of sorts, students not only gain knowledge from daily classroom
											teaching but also gather a world of experience from the museums, cultural festivals and
											innumerable sporting events. Home to world-class universities, Australia proffers courses
											in disciplines ranging from management studies, medicine, finance, literature, visual arts,
											astronomy, which is a fraction of an almost exhaustive list of subjects.</p>
									</div>
								</div>
							</div> 
							<!-- each panel -->
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i2">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i2" aria-expanded="false">
											How much will it cost to study in Australia?
											<span class="plus-minus"><span></span></span>
										</a>
									</h4>
								</div>
								<div id="ques-ans-i2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i2">
									<div class="panel-body">
										<p>On an average an international student may spend about AUD 360 per week
											accommodation, food, clothing, entertainment, transport and telephone. Do keep in mind
											that this figure depends on your style of living; location and also what course are you
											studying. However, you have to show that you have enough financials to cover your living
											cost, pay tuitions and your travel.</p>
									</div>
								</div>
							</div>
							<!-- each panel -->
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i3">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i3" aria-expanded="false">
										   Are Scholarships available for international students?
											<span class="plus-minus"><span></span></span>
										</a>

									</h4>
								</div>
								<div id="ques-ans-i3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i3">
									<div class="panel-body">
										<p>Yes. There are various scholarships available for international students. Though majority
										of these scholarships are available at Post Graduate level by Universities but some
										institutes also offer scholarships. Please check with your nearest VIEC office for an
										update.</p>
									</div>
								</div>
							</div>



							<!-- each panel -->
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i3">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i3" aria-expanded="false">
										   Is IELTS mandatory to study in Australia?
											<span class="plus-minus"><span></span></span>
										</a>

									</h4>
								</div>
								<div id="ques-ans-i3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i3">
									<div class="panel-body">
										<p>Yes. IELTS is the only test accepted by the Australian Department of Immigration and
										Citizenship (DIAC) for visa purposes.</p>
									</div>
								</div>
							</div>

							<!-- each panel -->
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i3">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i3" aria-expanded="false">
										   What type of accommodation is available for the students?
											<span class="plus-minus"><span></span></span>
										</a>

									</h4>
								</div>
								<div id="ques-ans-i3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i3">
									<div class="panel-body">
										<p>Students have a number of options available that can be on-campus or off-campus such
										as hostels, rented housing, home stay etc.</p>
									</div>
								</div>
							</div>

							<!-- each panel -->
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i3">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i3" aria-expanded="false">
										   Can I work in Australia?
											<span class="plus-minus"><span></span></span>
										</a>

									</h4>
								</div>
								<div id="ques-ans-i3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i3">
									<div class="panel-body">
										<p>Yes. International Students on a Student visa in Australia can work 20 hours in a week
											during course time and full time in vacations. However, you cannot start work until you
											have commenced your course in Australia.</p>
									</div>
								</div>
							</div>
														<!-- each panel -->
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i3">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i3" aria-expanded="false">
										   Can I extend my visa onshore?
											<span class="plus-minus"><span></span></span>
										</a>

									</h4>
								</div>
								<div id="ques-ans-i3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i3">
									<div class="panel-body">
										<p>Yes, you can do so however in some cases a student is granted a 'No further Stay' clause
										on their visas. In that case one has to come back and re-apply for a fresh student visa
										application from the home country.</p>
									</div>
								</div>
							</div>
							<!-- each panel -->
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="ques-i3">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse" data-parent="#general" href="#ques-ans-i3" aria-expanded="false">
										   Can my spouse or child accompany me in Australia while I am studying?
											<span class="plus-minus"><span></span></span>
										</a>

									</h4>
								</div>
								<div id="ques-ans-i3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ques-i3">
									<div class="panel-body">
										<p>Yes, they can apply for student dependent visa and meet the criteria to be granted a
										dependent visa</p>
									</div>
								</div>
							</div>

							<h3 class="color-primary">Additional information (Checklist for all the countries):</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Enim ad minim veniam quis nostrud exerci liquip ex modo.</p>
							<h3 class="color-primary">Embassy requirements:</h3>
							<p>-High school Transcript from Grade 7 to Grade 12 or 4 Year University
							Transcripts</p>
							<p>- Proof of English Proficiency</p>
							<p>- English Certificate (If applicable)</p>
							<p>- Birth certificate</p>
							<p>- Khmer national identification card of applicant</p>
							<p>- Family record book/ Resident record book</p>
							<p>- Parent’s business evidences</p>
							<p>- Patent license</p>
							<p>- Business registration</p>
							<p>- Civil servant ID card</p>
							<p>- Rental agreement (house, car, land, stall, etc.)</p>
							<p>- Property ownership certificate</p>
							<p>- Bank statement and bank transaction</p>
							<p>- Passport of applicant</p>
							<p>- Photo 5*5 with white background</p>
							<h3 class="color-primary">School requirements</h3>
							<p>- Academic Transcripts</p>
							<p>- Application fee</p>
							<p>- Admission forms</p>
							<p>- Passport</p>
							<p>- Bank settlements</p>

							<!-- each panel -->
						</div>
					</div>
					
					<!-- Sidebar -->
					<div class="col-md-4">
						<div class="sidebar-right">
							<div class="wgs-box wgs-menus">
								<div class="wgs-content">
									<ul class="list list-grouped">
										<li class="list-heading">
											<span>Visa Services</span>
											<ul>
												<li class="main-menu-right"><a>Visa Services</a>
													<ul class="sub-menu-right">
														<li><a href="service-single.html">United States of America</a></li>
														<li class="active"><a href="service-single-alter.html">Visa for Canada</a></li>
														<li><a href="service-single.html">Visa for New Zealand</a></li>
														<li><a href="service-single-alter.html">Visa for Singapore</a></li>
														<li><a href="service-single.html">Visa for United Kingdom</a></li>
														<li><a href="service-single-alter.html">Visa for USA</a></li>
													</ul>
												</li>
												<li class="main-menu-right"><a href="service-single-alter.html">Visa for Canada</a></li>
												<li><a href="service-single.html">Visa for New Zealand</a></li>
												<li><a href="service-single-alter.html">Visa for Singapore</a></li>
												<li><a href="service-single.html">Visa for United Kingdom</a></li>
												<li><a href="service-single-alter.html">Visa for USA</a></li>
											</ul>
										</li>
									</ul>									
								</div>
							</div>
							<div class="wgs-box boxed light has-bg has-bg-image">
								<div class="wgs-content">
									<h3>Innovative Tools for Investor</h3>
									<p>We employ a long-established strategy, sector-focused investing across all of our markets globally...</p>
									<a href="service-single.html" class="btn btn-alt btn-outline"> Learn More</a>
								</div>
								<div class="wgs-bg imagebg bg-image-loaded" style="background-image: url(&quot;image/photo-sd-a.jpg&quot;);">
									<img src="{{ asset('public/frontend/assets/image/photo-sd-a.jpg') }}" alt="">
								</div>
							</div>
							<div class="wgs-box boxed bg-secondary light">
								<div class="wgs-content">
									<h3>Need Help To Grow Your Business?</h3>
									<p>Investment Expert will help you start your own company.</p>
									<a href="contact.html" class="btn btn-light"> Get In Touch</a>
								</div>
							</div>

						</div>
					</div>
					<!-- Sidebar #end -->
				</div>
				
			</div>
		</div>		
	</div>
	@endsection