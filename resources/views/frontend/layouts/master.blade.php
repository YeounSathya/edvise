<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from edvise.asia/demo/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 22 Dec 2018 11:11:55 GMT -->
<head>
	<title>Welcome to Edvise</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="icon" href="{{ asset('public/frontend/assets/image/favicon.png" type="image/png" sizes="16x16') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/css/vendor.bundle.css') }}">
	<link id="style-css" rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/css/stylec64ec64e.css?ver=1.1.1') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/css/vendor.bundle.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/css/style.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/assets/camcyber/customise.css') }}">
	<link rel="shortcut icon" href="{{ asset ('public/frontend/assets/favicon.png') }}" type="image/x-icon">
</head>
<body class="site-body style-v1">
	<!-- Header --> 
	<header class="site-header header-s1 is-transparent is-sticky">
		<!-- Topbar -->
		<div class="topbar">
			<div class="container">
				<div class="row">
					<div class="top-aside top-left">
<!-- 						<ul class="top-nav">
							<li><a class="language" href="#">ខ្មែរ</a></li>
							<li><a class="language" href="#">EN</a></li>
						</ul> -->
					</div>
					<div class="top-aside top-right clearfix">
						<ul class="top-contact clearfix">
							<li class="t-email t-email1">
								<em class="fa fa-envelope-o" aria-hidden="true"></em>
								<span><a href="#">info@edvise.asia</a></span>
							</li>
							<li class="t-phone t-phone1">
								<em class="fa fa-phone" aria-hidden="true"></em>
								<span>+(855) 16 949 294</span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- #end Topbar -->
		<!-- Navbar -->
		<div class="navbar navbar-primary">
			<div class="container">
				<!-- Logo -->
					<a class="navbar-brand" href="index.html">
					<img class="logo logo-dark" alt="" src="{{ asset('public/frontend/assets/camcyber/logo.png') }}" srcset="{{ asset('public/frontend/assets/camcyber/logo.png') }}">
					<img class="logo logo-light" alt="" src="{{ asset('public/frontend/assets/camcyber/logo-light.png') }}" srcset="{{ asset('public/frontend/assets/camcyber/logo-light.png') }}">
				</a>
				<!-- #end Logo -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainnav" aria-expanded="false">
						<span class="sr-only">Menu</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Q-Button for Mobile -->
					<div class="quote-btn"><a class="btn" href="get-a-quote.html">Sign in</a></div>
				</div>
				<!-- MainNav -->
				<nav class="navbar-collapse collapse" id="mainnav">
					<ul class="nav navbar-nav">
						<li class="dropdown active"><a href="{{ route('home') }}">Home</a>
						</li>
						<li class="dropdown"><a href="#">About us <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{ route('director-speech')}}">Director Speech's</a></li>
								<li><a href="{{ route('management')}}">Management Team</a></li>
								<li><a href="{{ route('our-client')}}">Our Clients Say</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle">Services<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{ route('study-abroad') }}">Study Abroad</a></li>
								<li><a href="{{ route('visa-services')}}">Visa Servce</a></li>
								<li><a href="{{ route('education-advise')}}">Education advise</a></li>
								<li><a href="{{ route('bussiness-solution')}}">Business Solution</a></li>
							</ul>
						</li>
						<li><a href="{{ route('activities') }}">Activities</a></li>
						<li class="quote-btn"><a class="btn" href="{{ route('contact') }}">Contact Us</a></li>
					</ul>
				</nav>     
				<!-- #end MainNav -->
			</div>
		</div>
		<!-- #end Navbar -->
		<!-- Banner/Slider -->
	 
		<!-- #end Banner/Slider -->
	</header>
	<!-- End Header -->




	@yield('content')




	<!-- Footer Widget-->
	<div class="footer-widget style-v2 section-pad-md">
		<div class="container">
			<div class="row">

				<div class="widget-row row">
					<div class="footer-col col-md-3 col-sm-6 res-m-bttm">
						<!-- Each Widget -->
						<div class="wgs wgs-footer wgs-text">
							<div class="wgs-content">
								<p><img src="{{ asset('public/frontend/assets/camcyber/logo.png') }}"  alt=""></p>
								<p>Edvise is founded by a group of young and dynamic Cambodian entrepreneurs who have
more than a decade of experiences in education, travelling & visa arrangement and
career counseling. </p>
							</div>
						</div>
						<!-- End Widget -->
					</div>
					<div class="footer-col col-md-3 col-sm-6 col-md-offset-1 res-m-bttm">
						<!-- Each Widget -->
						<div class="wgs wgs-footer wgs-menu">
							<h5 class="wgs-title">Our Services</h5>
							<div class="wgs-content">
								<ul class="menu">
									<li><a href="{{ route('study-abroad') }}">Study Abroad</a></li>
									<li><a href="{{ route('visa-services')}}">Visa Servce</a></li>
									<li><a href="{{ route('education-advise')}}">Education advise</a></li>
									<li><a href="{{ route('bussiness-solution')}}">Business Solution</a></li>
								</ul>
							</div>
						</div>
						<!-- End Widget -->
					</div>
					<div class="footer-col col-md-2 col-sm-6 res-m-bttm">
						<!-- Each Widget -->
						<div class="wgs wgs-footer wgs-menu">
							<h5 class="wgs-title">About Us</h5>
							<div class="wgs-content">
								<ul class="menu">
									<li><a href="{{ route('director-speech')}}">Director Speech's</a></li>
									<li><a href="{{ route('management')}}">Management Team</a></li>
									<li><a href="{{ route('our-client')}}">Our Clients Say</a></li>
								</ul>
							</div>
						</div>
						<!-- End Widget -->
					</div>

					<div class="footer-col col-md-3 col-sm-6">
						<!-- Each Widget -->
						<div class="wgs wgs-footer">
							<h5 class="wgs-title">Get In Touch</h5>
							<div class="wgs-content">
								<p>CG06, St.579 Sk. Chroy Changva, Kh. Chroy Changva, Phnom Penh</p>
								<p>
									<span>Phone</span>: +(855) 16 949 294<br>
									<span>E-mail</span>: info@edvise.asia <br />
									<span>Website</span>: wwww.edvise.asia
								</p>
								<ul class="social">
									<li><a href="https://www.facebook.com/stom.sharow"><em class="fa fa-facebook" aria-hidden="true"></em></a></li>
									<li><a href="#"><em class="fa fa-twitter" aria-hidden="true"></em></a></li>
									<li><a href="#"><em class="fa fa-linkedin" aria-hidden="true"></em></a></li>
								</ul>
							</div>
						</div>
						<!-- End Widget -->
					</div>

				</div><!-- Widget Row -->

			</div>
		</div>
	</div>
	<!-- End Footer Widget -->

	<!-- Copyright -->
	<div class="copyright style-v2">
		<div class="container">
			<div class="row">
			
				<div class="row">
					<div class="site-copy col-sm-7">
						<p>Copyright &copy; 2018 Edvise Consulting Co., LTD. All Rights Reserved
					</div>
					<div class="site-by col-sm-5 al-right">
						<p>Designed by <a href="#" target="_blank">CamCyber</a></p>
					</div>
				</div>
				 				
			</div>
		</div>
	</div>
	<!-- End Copyright -->
	
	<!-- Rreload Image for Slider -->
	<div class="preload hide">
		<img alt="" src="{{ asset('public/frontend/assets/image/slider-lg-a.jpg') }}">
		<img alt="" src="{{ asset('public/frontend/assets/image/slider-lg-b.jpg') }}">
	</div>
	<!-- End -->

	<!-- Preloader !active please if you want -->
	<!-- <div id="preloader"><div id="status">&nbsp;</div></div> -->
	<!-- Preloader End -->

	<!-- JavaScript Bundle -->
	<script src="{{ asset('public/frontend/assets/js/jquery.bundle.js') }}"></script>
	<!-- Theme Script init() -->
	<script src="{{ asset('public/frontend/assets/js/script.js') }}"></script>
	<!-- End script -->
</body>

<!-- Mirrored from edvise.asia/demo/ by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 22 Dec 2018 11:12:48 GMT -->
</html>