<!-- Q-Button for Mobile -->
					<div class="quote-btn"><a class="btn" href="get-a-quote.html">Sign in</a></div>
				</div>
				<!-- MainNav -->
				<nav class="navbar-collapse collapse" id="mainnav">
					<ul class="nav navbar-nav">
						<li class="dropdown active"><a href="{{ route('home') }}">Home</a>
						</li>
						<li class="dropdown"><a href="#">About us <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{ route('director-speech')}}">Director Speech's</a></li>
								<li><a href="{{ route('management')}}">Management Team</a></li>
								<li><a href="{{ route('our-client')}}">Our Clients Say</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="service.html" class="dropdown-toggle">Services<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{ route('study-abroad') }}">Study Abroad</a></li>
								<li><a href="#">Visa Servce</a></li>
								<li><a href="#">Education advise</a></li>
								<li><a href="#">Business Solution</a></li>
							</ul>
						</li>
						<li><a href="{{ route('activities') }}">Activities</a></li>
						<li class="quote-btn"><a class="btn" href="{{ route('contact') }}">Contact Us</a></li>
					</ul>
				</nav>     
				<!-- #end MainNav -->
			</div>
		</div>
