	<!-- Banner/Slider -->

		<div id="slider" class="banner banner-slider slider-large carousel slide carousel-fade">
			<!-- Wrapper for Slides -->
			
			<div class="carousel-inner">
				@php($i = 1)
				@foreach ( $defaultData['slides'] as $row)
				<div class="item @if($i++ ==1) active @endif">				
					<!-- Set the first background image using inline CSS below. -->
					<div class="fill" style="background-image:url('{{ asset( $row->image) }}');">
						<div class="banner-content">
							<div class="container">
								<div class="row">
									<div class="banner-text al-left pos-left light">
										<h2>{{ $row->title }}<strong> </strong></h2>
										<p>We provide independent advice based on established research methods, and our experts have in-depth sector knowledge.</p>
										<a href="#" class="btn">Learn more</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
				
				<!-- <div class="item">
					<div class="fill" style="background-image:url('{{ asset('public/frontend/assets/image/slider-lg-b.jpg') }}');">
						<div class="banner-content">
							<div class="container">
								<div class="row">
									<div class="banner-text al-left pos-left light">
										<h2>Expert Study Advice<strong>.</strong></h2>
										<p>We help clients find ways to turn into actionable insights by embedding economics across their organization’s strategy.</p>
										<a href="#" class="btn">Learn more</a>
									</div>
								</div>
							</div>
						</div>					
					</div>
				</div>
			</div> -->
			<!-- Left and right controls -->
			<a class="left carousel-control" href="#slider" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#slider" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
		<!-- #end Banner/Slider -->
	