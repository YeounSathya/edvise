<?php

use Illuminate\Database\Seeder;


class ServiceTableSeeder extends Seeder
{
  
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    
	{
	     DB::table('service')->insert(
            [
                
                [ 
                    'title' => ' Study Abroad',
                    'is_published'          =>    1,
                ],
                [ 
                    'title' => 'Visa Service',
                    'is_published'          =>    1,
                ],

                 [ 
                    'title' => 'Eduction Adices ',
                    'is_published'          =>    1,
                ],

                 [ 
                    'title' => 'Business Solution',
                    'is_published'          =>    1,
                ],
                
               

            ]);

	}
}
