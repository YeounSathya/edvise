<?php

use Illuminate\Database\Seeder;


class ConstultingTableSeeder extends Seeder
{
  
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
	{

	     DB::table('constulting')->insert(
            [
                
                [ 
                    'title' => 'Study Abroad ',
                    'contents' => 'The advantages of overseas education are very high in numbers nowadays',
                    'image' => "public/frontend/assets/image/photo-pt-a.jpg",
                     'is_published'          =>    1,
                ],
                [ 
                    'title' => 'Visa Servce',
                    'contents' => 'Our visa service were established for various reasons such as to reunite ',
                    'image' => "public/frontend/assets/image/photo-pt-b.jpg",
                     'is_published'          =>    1,
                ],
                [ 
                    'title' => 'Education advise',
                    'contents' => 'Edvise Academy provides high quality English language training to both young',
                    'image' => "public/frontend/assets/image/photo-pt-c.jpg",
                     'is_published'          =>    1,
                ],
                [ 
                    'title' => 'Business Solutiont',
                    'contents' => 'We help our customers implement today  mission as well as adjust to and incorporate',
                    'image' => "public/frontend/assets/image/photo-pt-d.jpg",
                     'is_published'          =>    1,
                ],
                

            ]);

	   }
    }