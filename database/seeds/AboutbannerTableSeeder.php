<?php

use Illuminate\Database\Seeder;

class AboutbannerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('aboutbanner')->insert(
            [
                [
                    'title'                 => 'Portfolio',
                    'contents'              => 'Nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat do eiusmod tempor incidid.',
                    'name'                  => '',
                    'image'                 => 'public/frontend/assets/image/banner-inside-a.jpg',
                    'is_published'          =>    1,
                ],
                
                
               


                
            ]
        );
    }
}
