<?php

use Illuminate\Database\Seeder;

class DirectorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('director')->insert(
            [
                [
                    'title'                 => 'Mr. Kunthel Oum / Managing Director',
                    'contents'              => '',
                    'image'                 => 'public/frontend/assets/image/photo-pt-a.jpg',
                    'is_published'          =>    1,
                ],
                
               


                
            ]
        );
    }
}
