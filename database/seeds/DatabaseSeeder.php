<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        
        $this->call(PositionsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ManagementTableSeeder::class);
        $this->call(AboutbannerTableSeeder::class);
        $this->call(WhoweareTableSeeder::class);
        $this->call(OurclientTableSeeder::class);
        $this->call(DirectorTableSeeder::class);
        $this->call(ConstultingTableSeeder::class);
        $this->call(BannerfooterTableSeeder::class);
        $this->call(SlideTableSeeder::class);
        $this->call(OurmanagementTableSeeder::class);
        $this->call(PartnerTableSeeder::class);
        $this->call(OurupdateTableSeeder::class);
        $this->call(ServiceTableSeeder::class);
        $this->call(ServiceSpecificTableSeeder::class);
        


    }
}
