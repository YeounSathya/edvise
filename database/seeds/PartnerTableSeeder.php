<?php

use Illuminate\Database\Seeder;


class PartnerTableSeeder extends Seeder
{
  
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    
	{
	     DB::table('partner')->insert(
            [
                
                [ 
                    
                    'image' => "public/frontend/assets/image/cl-logo1-w.png",
                    'is_published'          =>    1,
                ],
                [ 
            
                    'image' => "public/frontend/assets/image/cl-logo2-w.png",
                    'is_published'          =>    1,
                ],

                [ 
            
                    'image' => "public/frontend/assets/image/cl-logo3-w.png",
                    'is_published'          =>    1,
                ],
               [ 
            
                    'image' => "public/frontend/assets/image/cl-logo4-w.png",
                    'is_published'          =>    1,
                ],
                [ 
            
                    'image' => "public/frontend/assets/image/cl-logo5-w.png",
                    'is_published'          =>    1,
                ],
                [ 
            
                    'image' => "public/frontend/assets/image/cl-logo6-w.png",
                    'is_published'          =>    1,
                ],

            ]);

	}
}
