<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatewhoweareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whoweare', function (Blueprint $table) {
            $table->increments('id', 11);
            $table->integer('data_order')->index()->nullable();
            $table->string('title', 550)->nullable();
            $table->string('name', 250)->nullable();
            $table->string('contents', 550)->nullable();
            $table->string('image', 250)->nullable();
            $table->boolean('is_published')->nullable();
           //The field that will appear for almost tables
            $table->integer('creator_id')->unsigned()->index()->nullable();
            $table->integer('updater_id')->unsigned()->index()->nullable();
            $table->integer('deleter_id')->unsigned()->index()->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whoweare');
    }
}
